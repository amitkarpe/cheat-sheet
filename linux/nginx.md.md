
- Install nginx on Ubuntu 18.04
```
sudo apt install -y nginx
sudo systemctl enable --now nginx
sudo systemctl is-active nginx
```

- configure host name
```
sudo cp /etc/hosts /etc/hosts.bak
sudo echo "127.0.0.1 example.com" >> /etc/hosts

curl -s example.com | head
```

- Self hosted
  - [Certificates for localhost](https://letsencrypt.org/docs/certificates-for-localhost/)
  - [How To Create a Self-Signed SSL Certificate for Nginx in Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-18-04)


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc0NTQxMDg5NiwxNjY4NTc5MTgsLTI4MD
I3MzU3NF19
-->
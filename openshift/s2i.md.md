

```
oc@dell:~$ oc status --suggest
In project new3 on server https://192.168.42.148:8443

svc/nginx - 172.30.22.66 ports 8080, 8443
  dc/nginx deploys openshift/nginx:1.12 
    deployment #1 failed 16 minutes ago: config change

svc/one - 172.30.226.155 ports 8080, 8443
  dc/one deploys openshift/nginx:1.12 
    deployment #1 running for 21 seconds - 0/1 pods (warning: 1 restarts)

Errors:
  * pod/one-1-zzp4l is crash-looping

    The container is starting and exiting repeatedly. This usually means the container is unable
    to start, misconfigured, or limited by security restrictions. Check the container logs with
    
      oc logs one-1-zzp4l -c one
    
    Current security policy prevents your containers from being run as the root user. Some images
    may fail expecting to be able to change ownership or permissions on directories. Your admin
    can grant you access to run containers that need to run as the root user with this command:
    
      oc adm policy add-scc-to-user anyuid -n new3 -z default
    

Info:
  * pod/nginx-1-deploy has no liveness probe to verify pods are still running.
    try: oc set probe pod/nginx-1-deploy --liveness ...
  * pod/one-1-deploy has no liveness probe to verify pods are still running.
    try: oc set probe pod/one-1-deploy --liveness ...
  * dc/nginx has no readiness probe to verify pods are ready to accept traffic or ensure deployment is successful.
    try: oc set probe dc/nginx --readiness ...
  * dc/nginx has no liveness probe to verify pods are still running.
    try: oc set probe dc/nginx --liveness ...
  * dc/one has no readiness probe to verify pods are ready to accept traffic or ensure deployment is successful.
    try: oc set probe dc/one --readiness ...
  * dc/one has no liveness probe to verify pods are still running.
    try: oc set probe dc/one --liveness ...

View details with 'oc describe <resource>/<name>' or list everything with 'oc get all'.
oc@dell:~$ oc get pod
NAME             READY     STATUS             RESTARTS   AGE
nginx-1-deploy   0/1       Error              0          16m
one-1-deploy     1/1       Running            0          44s
one-1-zzp4l      0/1       CrashLoopBackOff   2          39s
oc@dell:~$ oc get pod
NAME             READY     STATUS             RESTARTS   AGE
nginx-1-deploy   0/1       Error              0          16m
one-1-deploy     1/1       Running            0          52s
one-1-zzp4l      0/1       CrashLoopBackOff   2          47s
oc@dell:~$ oc logs one-1-zzp4l -c one
This is a S2I  centos base image:
To use it, install S2I: https://github.com/openshift/source-to-image
Sample invocation:
s2i build https://github.com/sclorg/nginx-container.git --context-dir=1.12/test/test-app/ centos/nginx-112-centos7 nginx-sample-app
You can then run the resulting image via:
docker run -p 8080:8080 nginx-sample-app
oc@dell:~$ logout
 dev@dell  ~  

```
> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbNTQwNTg5NzY0XX0=
-->
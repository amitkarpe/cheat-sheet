
- create ubuntu pod for basic testing
```bash
oc create deploymentconfig  ubuntu  --image ubuntu --  sleep 60000
```
- output
```console
$ oc create deploymentconfig  ubuntu  --image ubuntu --  sleep 60000
deploymentconfig.apps.openshift.io/ubuntu created

$ oc get pod ubuntu-1-cdt7p 
NAME             READY     STATUS    RESTARTS   AGE
ubuntu-1-cdt7p   1/1       Running   0          12s
$ oc rsh ubuntu-1-cdt7p                                             
# uname
Linux
# bash 
root@ubuntu-1-cdt7p:/# cat /etc/lsb-release 
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=18.04
DISTRIB_CODENAME=bionic
DISTRIB_DESCRIPTION="Ubuntu 18.04.3 LTS"
root@ubuntu-1-cdt7p:/# 

$ oc rsh ubuntu-1-cdt7p bash   
root@ubuntu-1-cdt7p:/# cat /etc/lsb-release 
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=18.04
DISTRIB_CODENAME=bionic
DISTRIB_DESCRIPTION="Ubuntu 18.04.3 LTS"
root@ubuntu-1-cdt7p:/# 


```

> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTkxMDk1ODU0MCwtMTc4MzgyMDkwMV19
-->
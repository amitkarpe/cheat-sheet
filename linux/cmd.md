# Remove all comments, and empty or whitespace-only lines. 
```
$ grep -Ev '^\s*(#|$)'  zsh_command_not_found 
if [[ -x /usr/lib/command-not-found ]] ; then
	if (( ! ${+functions[command_not_found_handler]} )) ; then
		function command_not_found_handler {
			[[ -x /usr/lib/command-not-found ]] || return 1
			/usr/lib/command-not-found --no-failure-msg -- ${1+"$1"} && :
		}
	fi

fi
```

- [copy into clipboard](https://stackoverflow.com/questions/5130968/how-can-i-copy-the-output-of-a-command-directly-into-my-clipboard)

```
sudo apt-get install xclip
```

You can then pipe the output into  `xclip`  to be copied into the clipboard:

```
cat file | xclip
```

To paste the text you just copied, you shall use:

```
xclip -o
```

To simplify life, you can set up an alias in your .bashrc file as I did:

```
alias "c=xclip"
alias "v=xclip -o"
```

To see how useful this is, imagine I want to open my current path in a new terminal window (there may be other ways of doing it like  Ctrl+T  on some systems, but this is just for illustration purposes):

```
Terminal 1:
pwd | c

Terminal 2:
cd `v`
```

Notice the  `` ` ` ``  around  `v`. This executes  `v`  as a command first and then substitutes it in-place for  `cd`  to use.

Only copy the content to the  `X`  clipboard

```
cat file | xclip
```

If you want to paste somewhere else other than a  `X`  application, try this one:

```
cat file | xclip -selection clipboard
```


```
echo "helloworld" | xsel -b
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwMDI2MTg4MzQsLTg5MTg2MzA0MF19
-->
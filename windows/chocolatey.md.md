
- setup choco using ps | [install](https://chocolatey.org/docs/installation)
```
Get-ExecutionPolicy
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

- setup choco using cmd
```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

- install 7zip on cmd with admin (to specific directory)
````
set ChocolateyInstall=c:\Users\P1328066\choco

choco -y install 7zip winrar evernote mobaxterm  chocolateygui
```

- List
- Dev -
```
choco install filezilla winscp curl vscode git.install openssh sublimetext3 jq
 
```
- User - 
```
choco install foxitreader teamviewer wget firefox firefoxesr    winrar 7zip.install vlc irfanview  greenshot googledrive google-drive-file-stream evernote 
```

- Advance User/ Vagrant
``` 
choco install -y kubernetes-cli  minikube docker-cli virtualbox vagrant packer vagrant-manager
mkdir vagrant/ubuntu
cd vagrant/ubuntu
vagrant init bento/ubuntu18.04
ls
vagrant up
```
- Admin
```

choco install cmder treesizefree autoruns cygwin  autohotkey
everything
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTA2NzE3NjQ2OCwyMDM1MzA2NTM3LC03Nz
A5MDcyOTMsMjg2NzYwMjg2LC03MjA5NDQ4MjAsLTc3ODMyNDA2
NiwtMTc0ODY1OTUzMiwtMTk3NDU1MTk4M119
-->
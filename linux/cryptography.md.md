
 - Video
   - [Breaking Down the TLS Handshake](https://www.youtube.com/watch?v=cuR05y_2Gxc)
   - [What's in a Digital Certificate?](https://www.youtube.com/watch?v=XmIlynkR8J8)


 - Wiki Info (Concept)
   + [PKI](https://en.wikipedia.org/wiki/Public_key_infrastructure)
   + [CA](https://en.wikipedia.org/wiki/Certificate_authority)
   + [Public key certificate](https://en.wikipedia.org/wiki/Public_key_certificate)

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMTcxNzc0NzAsLTE0NzcyMjkxMTNdfQ
==
-->
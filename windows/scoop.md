- install scoop

iwr -useb get.scoop.sh | iex

- install curl

scoop install curl

- [Example Setup Scripts](https://github.com/lukesampson/scoop/wiki/Example-Setup-Scripts) 
```bash
# utils
scoop install 7zip curl sudo git openssh coreutils grep sed less

# programming languages
scoop install python ruby go nodejs

# WAMP stack
scoop install apache mariadb php
iex (new-object net.webclient).downloadstring('https://gist.github.com/lukesampson/6546858/raw/apache-php-init.ps1')

# console theme
scoop install concfg pshazz
concfg import solarized small

# vim
scoop install vim
'
set ff=unix
set cindent
set tabstop=4
set shiftwidth=4
set expandtab
set backupdir=$TEMP
' | out-file ~/.vimrc -enc oem -append
```

- install git and openssh 
```
scoop install git openssh
[environment]::setenvironmentvariable('GIT_SSH', (resolve-path (scoop which ssh)), 'USER')

```

- more options
-  link https://github.com/lukesampson/scoop/wiki/SSH-on-Windows
```

C:\Users\P1328066\scoop>scoop search ssh
'main' bucket:
    git-with-openssh (2.24.0.windows.2)
    gow (0.8.0) --> includes 'ssh.bat'
    mls-software-openssh (8.1p1-1)
    openssh (7.6p1)
    ssh-copy-id (2015-03-22)
    win32-openssh (8.0.0.0p1-Beta)

```

- add bucket
```
scoop bucket add extras
```

- [Extra Bucket](https://github.com/lukesampson/scoop-extras/blob/master/bucket/mobaxterm.json)


- to do | https://github.com/lukesampson/scoop-extras
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjAyODAyMTczLC0xMTEzNTM0MTA3LC0xNj
IxMjI3MDksLTk1Nzg3Njk1LC0xNzA3MTEwODY1LDE5NTc3Njg1
OTRdfQ==
-->
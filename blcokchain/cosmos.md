- Cosmos Hub
  + Video
    - [Cosmos Proof of Stake - Sunny Aggrawal](https://www.youtube.com/watch?v=XxZ04w2x4nk) 
    - [Intro to the Cosmos Network ](https://www.youtube.com/watch?v=XAetXKTikLM)

  + Concept
    - [What is Tendermint?](https://tendermint.com/docs/introduction/introduction.html)
    
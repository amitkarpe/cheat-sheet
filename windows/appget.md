- install https://dl.appget.net/appget/appget.setup.exe

- run as admin to install packages
appget install filezilla

- Search
appget search file

- [Cygwin](https://appget.net/packages/i/cygwin)
``appget install cygwin``

- git
``appget install git``

- my list
appget install ditto lastpass notepad-plus-plus flux vlc evernote winrar
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5OTkxMTA2MiwyMDYyODkwNTAyLC0xMD
U1NDIyNzkzLC0xNDQyMjcyNjc4LC0xNzkyNjM5NDI4XX0=
-->